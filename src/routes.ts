interface IRoutes {
  main: string;
  books: {
    main: string;
    detail: string;
  };
  houses: {
    main: string;
    detail: string;
  };
  characters: {
    main: string;
    detail: string;
  };
}

export const routes: IRoutes = {
  main: '/',
  books: {
    main: '/books',
    detail: '/books/detail',
  },
  houses: {
    main: '/houses',
    detail: '/houses/detail',
  },
  characters: {
    main: '/characters',
    detail: '/characters/detail',
  },
};
