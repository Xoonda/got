import React from 'react';
import { Route, Switch } from 'react-router';
import './App.css';
import Header from './components/Header/Header';
import {
  Books,
  Characters,
  DetailBook,
  DetailHouses,
  Favorites,
  Houses,
  Main,
  DetailCharacter,
} from './pages';

const App: React.FC = () => {
  return (
    <div className='App'>
      <Header />
      <div className='content'>
        <Switch>
          <Route exact path='/' component={Main} />
          <Route exact path='/books' component={Books} />
          <Route exact path='/characters' component={Characters} />
          <Route exact path='/houses' component={Houses} />
          <Route exact path='/books/detail' component={DetailBook} />
          <Route exact path='/houses/detail' component={DetailHouses} />
          <Route exact path='/characters/detail' component={DetailCharacter} />
          <Route exact path='/favorites' component={Favorites} />
        </Switch>
      </div>
    </div>
  );
};

export default App;
