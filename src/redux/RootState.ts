import { ILastSeen } from '../components/Footer-lastSeen/LastSeen-interfaces';
import { IBookList } from '../pages/Books/Books-interfaces';
import { ICharList } from '../pages/Characters/characters-interfaces';
import { IFavoriteItems } from '../pages/Favorites/Favorites-interface';
import { IHousesList } from '../pages/Houses/Houses-interfaces';

export interface IRootState {
  book: {
    books: IBookList[];
    page: number;
    stopLoad: false;
  };
  houses: {
    houses: IHousesList[];
    housesPage: number;
    stopLoadHouses: boolean;
  };
  characters: {
    characters: ICharList[];
    page: number;
    load: boolean;
  };
  loading: {
    loading: boolean;
  };
  detailItem: {
    detailItem: IBookList;
  };
  detailtHouse: {
    detailItemHouse: IHousesList;
  };
  detailCharacter: {
    charItem: ICharList;
  };
  lastSeen: {
    items: ILastSeen;
  };
  favorites: {
    items: IFavoriteItems;
  };
}
