import axios from 'axios';
import { GET_CHARACTERS, NEXT_PAGE_CHARACTERS, STOP_LOAD_CHARACTERS } from '../../types';
import { addDetailItemCharacter } from '../detail/detailActions';
import { hideLoader, showLoader } from '../loader/loaderActions';

export const getCharacters = (page: number, name?: string, gender?: string) => {
  return async (dispatch: any) => {
    dispatch(showLoader());
    const config = {
      params: {
        page: page,
        name: name,
        gender: gender,
      },
    };
    const response = await axios.get(
      `https://www.anapioficeandfire.com/api/characters?pageSize=50`,
      config,
    );
    if (response.data.length < 50) {
      dispatch(stopLoadCharacters());
    }
    if (response.data.length) {
      dispatch({ type: GET_CHARACTERS, payload: response.data });
      dispatch({ type: NEXT_PAGE_CHARACTERS });
      dispatch(hideLoader());
    } else {
      dispatch(hideLoader());
      return null;
    }
  };
};

export const getUniqeCharacter = (url: string) => {
  return async (dispatch: any) => {
    dispatch(showLoader());
    const response = await axios.get(url);
    dispatch(addDetailItemCharacter(response.data));
    dispatch(hideLoader());
  };
};

export const nextPageCharacters = (page: number) => ({ type: NEXT_PAGE_CHARACTERS });
export const stopLoadCharacters = () => ({ type: STOP_LOAD_CHARACTERS });
