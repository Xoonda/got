import { IBookList } from '../../../pages/Books/Books-interfaces';
import { ICharList } from '../../../pages/Characters/characters-interfaces';
import { IHousesList } from '../../../pages/Houses/Houses-interfaces';
import { TOGGLE_FAVORITE_ITEM } from '../../types';

export const toggleFavoriteItem = (item: IBookList | IHousesList | ICharList) => ({
  type: TOGGLE_FAVORITE_ITEM,
  payload: item,
});
