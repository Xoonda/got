import axios from 'axios';
import { GET_HOUSES, NEXT_PAGE_HOUSES, STOP_LOAD_HOUSES } from '../../types';
import { hideLoader, showLoader } from '../loader/loaderActions';

export const getHouses = (page: number) => {
  return async (dispatch: any) => {
    dispatch(showLoader());
    const response = await axios.get(`https://www.anapioficeandfire.com/api/houses?page=${page}`);
    if (response.data.length < 10) {
      dispatch(stopLoadHouses());
    }
    dispatch(hideLoader());
    if (response.data.length) {
      dispatch({ type: GET_HOUSES, payload: response.data });
      dispatch({ type: NEXT_PAGE_HOUSES });
    } else {
      return null;
    }
  };
};

export const nexPageHouses = (page: number) => ({ type: NEXT_PAGE_HOUSES });
export const stopLoadHouses = () => ({ type: STOP_LOAD_HOUSES });
