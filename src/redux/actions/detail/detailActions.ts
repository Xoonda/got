import { IBookList } from '../../../pages/Books/Books-interfaces';
import { ICharList } from '../../../pages/Characters/characters-interfaces';
import { IHousesList } from '../../../pages/Houses/Houses-interfaces';
import {
  DETAIL_ITEM_BOOK_ADD,
  DETAIL_ITEM_CHARACTER_ADD,
  DETAIL_ITEM_HOUSE_ADD,
} from '../../types';

export const addDetailItemBook = (item: IBookList) => ({
  type: DETAIL_ITEM_BOOK_ADD,
  payload: item,
});

export const addDetailItemHouse = (item: IHousesList) => ({
  type: DETAIL_ITEM_HOUSE_ADD,
  payload: item,
});

export const addDetailItemCharacter = (item: ICharList) => ({
  type: DETAIL_ITEM_CHARACTER_ADD,
  payload: item,
});
