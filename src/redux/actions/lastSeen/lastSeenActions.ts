import { IBookList } from '../../../pages/Books/Books-interfaces';
import { ICharList } from '../../../pages/Characters/characters-interfaces';
import { IHousesList } from '../../../pages/Houses/Houses-interfaces';
import { LAST_SEEN_ITEM_ADD } from '../../types';

export const addLastSeenItem = (item: IBookList | IHousesList | ICharList) => ({
  type: LAST_SEEN_ITEM_ADD,
  payload: item,
});
