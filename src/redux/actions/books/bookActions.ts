import axios from 'axios';
import { GET_BOOKS, NEXT_PAGE_BOOKS, STOP_LOAD_BOOKS } from '../../types';
import { hideLoader, showLoader } from '../loader/loaderActions';

export const getBooks = (pageNumber: number) => {
  return async (dispatch: any) => {
    dispatch(showLoader());
    const response = await axios.get(
      `https://www.anapioficeandfire.com/api/books?page=${pageNumber}`,
    );
    if (response.data.length < 10) {
      dispatch(stopLoadForBooks());
    }
    dispatch(hideLoader());
    if (response.data.length) {
      dispatch({ type: GET_BOOKS, payload: response.data });
      dispatch({ type: NEXT_PAGE_BOOKS });
    } else {
      return null;
    }
  };
};

export const nextPageForBooks = () => ({ type: NEXT_PAGE_BOOKS });
export const stopLoadForBooks = () => ({ type: STOP_LOAD_BOOKS });
