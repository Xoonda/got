import { lastSeenReducer } from './lastSeen/lastSeenReducer';
import { combineReducers } from 'redux';
import { bookReducer } from './books/bookReducer';
import { charReducer } from './characters/charactersReducer';
import { detailItemCharacterReducer } from './characters/characterItemDetail';
import { DetailItemReducer } from './houses/houseDeteilItem';
import { detailReducer } from './books/bookDetailReducer';
import { houseReducer } from './houses/housesReducer';
import { loadinReducer } from './loading/loadingReducer';
import { favoritesReducer } from './favorite/favoriteReducer';

export const rootReducer = combineReducers({
  book: bookReducer,
  houses: houseReducer,
  characters: charReducer,
  loading: loadinReducer,
  detailItem: detailReducer,
  detailtHouse: DetailItemReducer,
  detailCharacter: detailItemCharacterReducer,
  lastSeen: lastSeenReducer,
  favorites: favoritesReducer,
});
