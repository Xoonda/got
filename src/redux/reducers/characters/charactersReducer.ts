import { ICharList } from '../../../pages/Characters/characters-interfaces';
import { GET_CHARACTERS, NEXT_PAGE_CHARACTERS, STOP_LOAD_CHARACTERS } from '../../types';

interface ICharState {
  characters: ICharList[];
  page: number;
  stopLoad: boolean;
}

interface ICharAction {
  type: string;
  payload: ICharList[];
}

const initialState: ICharState = {
  characters: [],
  page: 1,
  stopLoad: false,
};

export const charReducer = (state = initialState, action: ICharAction) => {
  switch (action.type) {
    case GET_CHARACTERS:
      return { ...state, characters: [...state.characters, ...action.payload] };
    case NEXT_PAGE_CHARACTERS:
      return { ...state, page: state.page + 1 };
    case STOP_LOAD_CHARACTERS:
      return { ...state, load: true };
    default:
      return state;
  }
};
