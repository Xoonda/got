import { ICharList } from '../../../pages/Characters/characters-interfaces';
import { DETAIL_ITEM_CHARACTER_ADD } from '../../types';

interface ICharItemState {
  charItem: ICharList;
}

interface IAction {
  type: string;
  payload: ICharList;
}

const initialState: ICharItemState = {
  charItem: {
    url: '',
    name: '',
    gender: '',
    culture: '',
    born: '',
    died: '',
    titles: [],
    aliases: [],
    father: '',
    mother: '',
    spouse: '',
    allegiances: [],
    books: [],
    povBooks: [],
    tvSeries: [],
    playedBy: [],
  },
};

export const detailItemCharacterReducer = (state = initialState, action: IAction) => {
  switch (action.type) {
    case DETAIL_ITEM_CHARACTER_ADD:
      return { ...state, charItem: action.payload };
    default:
      return state;
  }
};
