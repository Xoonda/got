import { IBookList, IDetailItem } from '../../../pages/Books/Books-interfaces';
import { DETAIL_ITEM_BOOK_ADD } from '../../types';

interface IDetailAction {
  type: string;
  payload: IBookList;
}

const initialState: IDetailItem = {
  detailItemBook: {
    url: '',
    name: '',
    isbn: '',
    authors: [],
    numberOfPages: 0,
    publisher: '',
    country: '',
    mediaType: '',
    released: '',
    characters: [],
    povCharacters: [],
  },
};

export const detailReducer = (state: IDetailItem = initialState, action: IDetailAction) => {
  switch (action.type) {
    case DETAIL_ITEM_BOOK_ADD:
      return { ...state, detailItem: action.payload };
    default:
      return state;
  }
};
