import { IBookList } from '../../../pages/Books/Books-interfaces';
import { GET_BOOKS, NEXT_PAGE_BOOKS, STOP_LOAD_BOOKS } from '../../types';
interface IBooksState {
  books: IBookList[];
  page: number;
  stopLoad: boolean;
}
interface IBookAction {
  type: string;
  payload: IBookList[];
}
const initialState: IBooksState = {
  books: [],
  page: 1,
  stopLoad: false,
};

export const bookReducer = (state: IBooksState = initialState, action: IBookAction) => {
  switch (action.type) {
    case GET_BOOKS:
      return { ...state, books: [...state.books, ...action.payload] };
    case NEXT_PAGE_BOOKS:
      return { ...state, page: state.page + 1 };
    case STOP_LOAD_BOOKS:
      return { ...state, stopLoad: true };
    default:
      return state;
  }
};
