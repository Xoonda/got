import { IHousesList } from '../../../pages/Houses/Houses-interfaces';
import { GET_HOUSES, NEXT_PAGE_HOUSES, STOP_LOAD_HOUSES } from '../../types';

interface IHousesState {
  houses: IHousesList[];
  housesPage: number;
  stopLoadHouses: boolean;
}

interface IHousesAction {
  type: string;
  payload: IHousesList[];
}

const initialState: IHousesState = {
  houses: [],
  housesPage: 1,
  stopLoadHouses: false,
};

export const houseReducer = (state: IHousesState = initialState, action: IHousesAction) => {
  switch (action.type) {
    case GET_HOUSES:
      return { ...state, houses: [...state.houses, ...action.payload] };
    case NEXT_PAGE_HOUSES:
      return { ...state, housesPage: state.housesPage + 1 };
    case STOP_LOAD_HOUSES:
      return { ...state, stopLoadHouses: true };
    default:
      return state;
  }
};
