import { IHousesList } from '../../../pages/Houses/Houses-interfaces';
import { DETAIL_ITEM_HOUSE_ADD } from '../../types';

interface IAction {
  type: string;
  payload: IHousesList;
}

interface IDetailItemHouseState {
  detailItemHouse: IHousesList;
}

const initialState: IDetailItemHouseState = {
  detailItemHouse: {
    url: '',
    name: '',
    region: '',
    coatOfArms: '',
    words: '',
    titles: [],
    seats: [],
    currentLord: '',
    heir: '',
    overlord: '',
    founded: '',
    founder: '',
    diedOut: '',
    ancestralWeapons: [],
    cadetBranches: [],
    swornMembers: [],
  },
};

export const DetailItemReducer = (state = initialState, action: IAction) => {
  switch (action.type) {
    case DETAIL_ITEM_HOUSE_ADD:
      return { ...state, detailItemHouse: action.payload };
    default:
      return state;
  }
};
