import { SHOW_LOADER, HIDE_LOADER } from '../../types';

export interface ILoadingState {
  loading: boolean;
}

export interface ILoadingAction {
  type: string;
}

const initialState: ILoadingState = {
  loading: false,
};

export const loadinReducer = (state: ILoadingState = initialState, action: ILoadingAction) => {
  switch (action.type) {
    case SHOW_LOADER:
      return { ...state, loading: true };
    case HIDE_LOADER:
      return { ...state, loading: false };
    default:
      return state;
  }
};
