import { Map } from 'immutable';
import { IBookList } from '../../../pages/Books/Books-interfaces';
import { ICharList } from '../../../pages/Characters/characters-interfaces';
import { IHousesList } from '../../../pages/Houses/Houses-interfaces';
import { TOGGLE_FAVORITE_ITEM } from '../../types';
interface IState {
  items: Map<IBookList['url'], IBookList | IHousesList | ICharList>;
}
interface IAction {
  type: typeof TOGGLE_FAVORITE_ITEM;
  payload: IBookList | IHousesList | ICharList;
}
const initialState: IState = {
  items: Map(),
};

export const favoritesReducer = (state = initialState, { type, payload }: IAction) => {
  switch (type) {
    case TOGGLE_FAVORITE_ITEM:
      return {
        ...state,
        items: state.items.has(payload.url)
          ? state.items.delete(payload.url)
          : state.items.set(payload.url, payload),
      };
    default:
      return state;
  }
};
