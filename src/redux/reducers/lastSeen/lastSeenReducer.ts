import { IBookList } from '../../../pages/Books/Books-interfaces';
import { ICharList } from '../../../pages/Characters/characters-interfaces';
import { IHousesList } from '../../../pages/Houses/Houses-interfaces';
import { LAST_SEEN_ITEM_ADD } from '../../types';

interface IItemsState {
  items: Array<IBookList | IHousesList | ICharList>;
}

interface IAction {
  type: string;
  payload: IBookList | IHousesList | ICharList;
}

const initialState: IItemsState = {
  items: [],
};

export const lastSeenReducer = (state = initialState, action: IAction) => {
  switch (action.type) {
    case LAST_SEEN_ITEM_ADD:
      return { ...state, items: [action.payload, ...state.items] };
    default:
      return state;
  }
};
