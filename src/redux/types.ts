// ============BOOKS================//
export const GET_BOOKS = 'GET_BOOKS';
export const NEXT_PAGE_BOOKS = 'NEXT_PAGE_BOOKS';
export const STOP_LOAD_BOOKS = 'STOP_LOAD_BOOKS';
// =============HOUSES==============//
export const GET_HOUSES = 'GET_HOUSES';
export const NEXT_PAGE_HOUSES = 'NEXT_PAGE_HOUSES';
export const STOP_LOAD_HOUSES = 'STOP_LOAD_HOUSES';
//===========CHARECTERS==========//
export const GET_CHARACTERS = 'GET_CHARACTERS';
export const NEXT_PAGE_CHARACTERS = 'NEXT_PAGE_CHARACTERS';
export const STOP_LOAD_CHARACTERS = 'STOP_LOAD_CHARACTERS';
export const GET_UNIQE_CHARACTER = 'GET_UNIQE_CHARACTER';
//==============LOADER==============//
export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';
// ==============DETAIL==============//
export const DETAIL_ITEM_BOOK_ADD = 'DETAIL_ITEM_BOOK_ADD';
export const DETAIL_ITEM_HOUSE_ADD = 'DETAIL_ITEM_HOUSE_ADD';
export const DETAIL_ITEM_CHARACTER_ADD = 'DETAIL_ITEM_CHARACTER_ADD';
// ==============LastSeenBlock=======//
export const LAST_SEEN_ITEM_ADD = 'LAST_SEEN_ITEM_ADD';
// ==============Favorites===========//
export const TOGGLE_FAVORITE_ITEM = 'TOGGLE_FAVORITE_ITEM';
