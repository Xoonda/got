import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CardForBook from '../../components/Cards/BookCard/CardForBook';
import Spiner from '../../components/Spiner/Spiner';
import { getBooks } from '../../redux/actions/books/bookActions';
// import useScroll from "../customHooks/useScroll";
import { IRootState } from '../../redux/RootState';
import { IBookList } from './Books-interfaces';

export const Books: React.FC = () => {
  const dispatch = useDispatch();
  const favoritesItems = useSelector((state: IRootState) => state.favorites.items);
  const load = useSelector((state: IRootState) => state.loading.loading);
  const books = useSelector((state: IRootState) => state.book.books);
  const page = useSelector((state: IRootState) => state.book.page);
  const stopLoad = useSelector((state: IRootState) => state.book.stopLoad);

  // const scrolling = useScroll(parentRef, childRef, () => dispatch(getBooks(page)))
  useEffect(() => {
    page === 1 && !stopLoad && dispatch(getBooks(page));
  }, [page, stopLoad, dispatch]);
  if (load) {
    return <Spiner />;
  }
  return (
    <div className='books-page'>
      <div className='books-list'>
        {books.map((book: IBookList, index: number) => {
          return <CardForBook key={index} item={book} isFavorite={favoritesItems.has(book.url)} />;
        })}
      </div>

      {!stopLoad && (
        <button
          onClick={() => dispatch(getBooks(page))}
          type='button'
          className='btn btn-secondary'>
          Загрузить ещё
        </button>
      )}
    </div>
  );
};
