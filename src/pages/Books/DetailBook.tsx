import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { IRootState } from '../../redux/RootState';
import { CharactersListExtend } from '../Characters';
import { IBookList } from './Books-interfaces';

export const DetailBook: React.FC = () => {
  const item: IBookList = useSelector((state: IRootState) => state.detailItem.detailItem);
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };
  return (
    <div className='detail-book-page'>
      <button onClick={goBack} type='button' className='btn btn-secondary go-back'>
        Назад
      </button>
      <h2>Название книги: {item.name}</h2>
      <h4>
        Автор: <i>{item.authors.join(', ')}</i>
      </h4>
      <p>
        Страна: <i>{item.country}</i>
      </p>
      <p>
        Число страниц: <i>{item.numberOfPages}</i>
      </p>
      <p>
        Дата релиза: <i>{item.released}</i>
      </p>
      <p>
        Издатель: <i>{item.publisher}</i>
      </p>
      <p>Список персонажей:</p>
      <CharactersListExtend urls={item.characters} />
    </div>
  );
};
