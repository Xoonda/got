import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';
import CardForBook from '../../components/Cards/BookCard/CardForBook';
import CardForChar from '../../components/Cards/CharacterCard/CardForChar';
import CardForHouses from '../../components/Cards/HouseCard/CardForHouses';
import { IRootState } from '../../redux/RootState';
import { IBookList } from '../Books/Books-interfaces';
import { ICharList } from '../Characters/characters-interfaces';
import { IHousesList } from '../Houses/Houses-interfaces';

export const Favorites: React.FC = () => {
  const items = useSelector((state: IRootState) => state.favorites.items);
  const render = useCallback((item, index) => {
    if (item.url.indexOf('/book') + 1) {
      console.log(item);
      return <CardForBook item={item as IBookList} isFavorite={true} key={index} />;
    } else if (item.url.indexOf('/houses') + 1) {
      return <CardForHouses item={item as IHousesList} isFavorite={true} key={index} />;
    } else {
      return <CardForChar item={item as ICharList} isFavorite={true} key={index} />;
    }
  }, []);
  return <div className='favorites-page'>{[...items.values()].map(render)}</div>;
};
