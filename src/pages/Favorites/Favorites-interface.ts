import { IBookList } from '../Books/Books-interfaces';
import { ICharList } from '../Characters/characters-interfaces';
import { IHousesList } from '../Houses/Houses-interfaces';

export type IFavoriteItems = Map<IBookList['url'], IBookList | IHousesList | ICharList>;
