import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CardForHouses from '../../components/Cards/HouseCard/CardForHouses';
import { IRootState } from '../../redux/RootState';
import Spiner from '../../components/Spiner/Spiner';
import { IHousesList } from './Houses-interfaces';
import { getHouses } from '../../redux/actions/houses/houseActions';

export const Houses: React.FC = () => {
  const dispatch = useDispatch();
  const stopLoad = useSelector((state: IRootState) => state.houses.stopLoadHouses);
  const load = useSelector((state: IRootState) => state.loading.loading);
  const pageNumber = useSelector((state: IRootState) => state.houses.housesPage);
  const houses = useSelector((state: IRootState) => state.houses.houses);
  const favoritesItems = useSelector((state: IRootState) => state.favorites.items);
  useEffect(() => {
    pageNumber === 1 && !stopLoad && dispatch(getHouses(pageNumber));
  }, [pageNumber, stopLoad, dispatch]);
  if (load) {
    return <Spiner />;
  }

  return (
    <div className='houses-wrapper'>
      <div className='houses-cards'>
        {houses.map((item: IHousesList, index: number) => {
          return (
            <CardForHouses item={item} key={index} isFavorite={favoritesItems.has(item.url)} />
          );
        })}
      </div>
      {!stopLoad && (
        <button
          onClick={() => dispatch(getHouses(pageNumber))}
          type='button'
          className='btn btn-secondary'>
          Загрузить ещё
        </button>
      )}
    </div>
  );
};
