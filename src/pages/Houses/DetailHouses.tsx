import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { IRootState } from '../../redux/RootState';
import { IHousesList } from './Houses-interfaces';

export const DetailHouses: React.FC = () => {
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };
  const item: IHousesList = useSelector((state: IRootState) => state.detailtHouse.detailItemHouse);
  return (
    <div className='houses-detail'>
      <button onClick={goBack} type='button' className='btn btn-secondary go-back'>
        Назад
      </button>
      <h2>Название дома: {item.name}</h2>
      <h4>
        Регион: <i>{item.region}</i>
      </h4>
      <p>
        Описание герба: <i>{item.coatOfArms}</i>
      </p>
      <p>
        Девиз: <i>{item.words}</i>
      </p>
      <p>
        Титулы <i>{item.titles.join(', ')}</i>
      </p>
      <p>
        Земли <i>{item.seats.join(', ')}</i>
      </p>
      <p>
        Основан в <i>{item.founded}</i>
      </p>
    </div>
  );
};
