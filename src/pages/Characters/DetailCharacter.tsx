import React from 'react';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router';
import { IRootState } from '../../redux/RootState';
import { ICharList } from './characters-interfaces';

export const DetailCharacter: React.FC = () => {
  const history = useHistory();
  const goBack = () => {
    history.goBack();
  };
  const item: ICharList = useSelector((state: IRootState) => state.detailCharacter.charItem);
  return (
    <div className='character-detail'>
      <button onClick={goBack} type='button' className='btn btn-secondary go-back'>
        Назад
      </button>
      <h2>
        Имя: <i>{item.name}</i>{' '}
      </h2>
      <h4>
        Пол: <i>{item.gender}</i>
      </h4>
      <p>
        Культура: <i>{item.culture}</i>
      </p>
      <p>
        Рожден: <i>{item.born}</i>
      </p>
      <p>
        Умер: <i>{item.died}</i>
      </p>
      <p>
        Титулы: <i>{item.titles.join(', ')}</i>
      </p>
      <p>
        Псевдонимы: <i>{item.aliases.join(', ')}</i>
      </p>
      <p>
        Отец\Мать:{' '}
        <i>
          {item.father}, {item.father}
        </i>
      </p>
      <p>
        Исполнен: <i>{item.playedBy.join(', ')}</i>
      </p>
    </div>
  );
};
