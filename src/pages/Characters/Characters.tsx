import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CardForChar from '../../components/Cards/CharacterCard/CardForChar';
import Dropdown from '../../components/Search/Dropdown';
import Input from '../../components/Search/Input';
import Spiner from '../../components/Spiner/Spiner';
import { getCharacters } from '../../redux/actions/characters/characterActions';
import { IRootState } from '../../redux/RootState';
import { ICharList } from './characters-interfaces';

export const Characters: React.FC = () => {
  const [inputValue, setInputValue] = useState('');
  const [selectValue, setSelectValue] = useState('');
  const dispatch = useDispatch();
  const page = useSelector((state: IRootState) => state.characters.page);
  const characters = useSelector((state: IRootState) => state.characters.characters);
  const stopLoad = useSelector((state: IRootState) => state.characters.load);
  const load = useSelector((state: IRootState) => state.loading.loading);
  const favoritesItems = useSelector((state: IRootState) => state.favorites.items);

  const changeInput = (event: React.ChangeEvent<HTMLInputElement>) =>
    setInputValue(event.target.value);

  const changeSelect = (event: React.ChangeEvent<HTMLSelectElement>) =>
    setSelectValue(event.target.value);
  const filterItem = () => dispatch(getCharacters(page, inputValue, selectValue));
  console.log(inputValue, selectValue);

  useEffect(() => {
    page === 1 && !stopLoad && dispatch(getCharacters(page));
  }, [page, stopLoad, dispatch]);

  if (load) return <Spiner />;
  return (
    <div>
      <div className='input-group mb-3'>
        <Input value={inputValue} changeInput={changeInput} />
        <Dropdown value={selectValue} changeSelect={changeSelect} />
        <button type='button' className='btn btn-secondary' onClick={filterItem}>
          Найти
        </button>
      </div>

      <div className='char-cards'>
        {characters.map((item: ICharList, number: number) => {
          if (item.name) {
            return (
              <CardForChar item={item} key={number} isFavorite={favoritesItems.has(item.url)} />
            );
          } else {
            return null;
          }
        })}
        <br />
      </div>
      {!stopLoad && (
        <button
          onClick={() => dispatch(getCharacters(page))}
          type='button'
          className='btn btn-secondary'>
          Загрузить ещё
        </button>
      )}
    </div>
  );
};
