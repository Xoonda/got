import React from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { getUniqeCharacter } from '../../redux/actions/characters/characterActions';

interface ICharListExt {
  urls: string[];
}

export const CharactersListExtend: React.FC<ICharListExt> = ({ urls }) => {
  const dispatch = useDispatch();
  return (
    <div className='char-list-ext'>
      {urls.map((url, index) =>
        index < 10 ? (
          <NavLink to='/characters/detail' key={index}>
            <p className='char-ext' onClick={() => dispatch(getUniqeCharacter(url))}>
              {index}.{url}
            </p>
          </NavLink>
        ) : null,
      )}
    </div>
  );
};
