import React from 'react';
import CardForMain from '../../components/Cards/MainPageCard/CardForMain';
import logoBook from '../../img/book.svg';
import logoHouses from '../../img/house.svg';
import logoChar from '../../img/characters.svg';
import LastSeen from '../../components/Footer-lastSeen/LastSeen';

export const Main: React.FC = () => {
  return (
    <div>
      <div className='main'>
        <CardForMain link={'/books'} name={'Book'} logo={logoBook} />
        <CardForMain link={'/houses'} name={'Houses'} logo={logoHouses} />
        <CardForMain link={'/characters'} name={'Characters'} logo={logoChar} />
      </div>
      <LastSeen />
    </div>
  );
};
