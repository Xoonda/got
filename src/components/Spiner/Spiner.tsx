import React from 'react';

const Spiner: React.FC = () => {
  return (
    <div className='spiner'>
      <div className='spinner-border text-secondary' role='status'>
        <span className='visually-hidden'>Loading...</span>
      </div>
    </div>
  );
};

export default Spiner;
