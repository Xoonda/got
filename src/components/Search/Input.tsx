import React from 'react';
interface IInput {
  value: string;
  changeInput(event: React.ChangeEvent<HTMLInputElement>): void;
}
const Input: React.FC<IInput> = ({ value, changeInput }) => {
  return (
    <input
      type='text'
      className='form-control'
      aria-label='Text input with dropdown button'
      value={value}
      onChange={changeInput}
    />
  );
};

export default Input;
