import React from 'react';
interface IDropdown {
  value: string;
  changeSelect(event: React.ChangeEvent<HTMLSelectElement>): void;
}
const Dropdown: React.FC<IDropdown> = ({ value, changeSelect }) => {
  return (
    <select
      value={value}
      onChange={changeSelect}
      className='form-select'
      aria-label='Default select example'>
      <option value='0'>Выберите пол</option>
      <option value='Male'>Мужской</option>
      <option value='Female'>Женский</option>
    </select>
  );
};

export default Dropdown;
