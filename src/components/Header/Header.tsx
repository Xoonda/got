import React from 'react';
import logo from '../../img/got-logo.svg';
import { NavLink } from 'react-router-dom';

const Header: React.FC = () => {
  return (
    <div className='header'>
      <img className='header-logo' src={logo} alt='' />

      <div className='header-nav'>
        <NavLink to='/'>
          <button type='button' className='btn btn-dark'>
            Главная
          </button>
        </NavLink>
        <NavLink to='/books'>
          <button type='button' className='btn btn-dark'>
            Книги
          </button>
        </NavLink>
        <NavLink to='/houses'>
          <button type='button' className='btn btn-dark'>
            Дома
          </button>
        </NavLink>
        <NavLink to='/characters'>
          <button type='button' className='btn btn-dark'>
            Персонажи
          </button>
        </NavLink>
        <NavLink to='/favorites'>
          <button type='button' className='btn btn-dark'>
            Избранное
          </button>
        </NavLink>
      </div>
    </div>
  );
};

export default Header;
