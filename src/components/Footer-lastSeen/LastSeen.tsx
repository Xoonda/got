import React from 'react';
import { useSelector } from 'react-redux';
import { IBookList } from '../../pages/Books/Books-interfaces';
import { ICharList } from '../../pages/Characters/characters-interfaces';
import { IHousesList } from '../../pages/Houses/Houses-interfaces';
import { IRootState } from '../../redux/RootState';
import CardForLastSeen from '../Cards/LastSeenCard/CardForLastSeen';

const LastSeen: React.FC = () => {
  const items = useSelector((state: IRootState) => state.lastSeen.items);
  if (!items.length) {
    return (
      <div className='last-seen'>
        <p className='text-center'>
          <span className='last-seen-header'>Последние просмотры</span>
        </p>
        <p className='text-center'>Вы пока ничего не просматривали</p>
      </div>
    );
  }

  return (
    <div className='last-seen'>
      <p className='text-center'>Последние просмотры</p>
      <div className='last-seen-wrapper'>
        {items.map((item, number) => {
          const description = [];
          let category = '';
          if (item.url.indexOf('/books') + 1) {
            category = 'Books';
            description.push(
              (item as IBookList).authors,
              (item as IBookList).country,
              (item as IBookList).publisher,
              (item as IBookList).numberOfPages + ' pages',
            );
          } else if (item.url.indexOf('/houses') + 1) {
            category = 'Houses';
            description.push(
              (item as IHousesList).region,
              (item as IHousesList).coatOfArms,
              (item as IHousesList).titles,
            );
          } else {
            category = 'Charecters';
            description.push(
              (item as ICharList).gender,
              (item as ICharList).culture,
              (item as ICharList).born,
              (item as ICharList).playedBy,
            );
          }

          if (number < 4) {
            return (
              <CardForLastSeen
                key={number}
                name={item.name}
                category={category}
                description={description.join(', ')}
              />
            );
          } else {
            return null;
          }
        })}
      </div>
    </div>
  );
};

export default LastSeen;
