import { IBookList } from '../../pages/Books/Books-interfaces';
import { ICharList } from '../../pages/Characters/characters-interfaces';
import { IHousesList } from '../../pages/Houses/Houses-interfaces';

export type ILastSeen = Array<IBookList | IHousesList | ICharList>;
