import React from 'react';

interface ICardForLastSeen {
  name: string;
  category: string;
  description: string;
}

const CardForLastSeen: React.FC<ICardForLastSeen> = ({ name, category, description }) => {
  return (
    <div className='card'>
      <div className='card-body'>
        <h5 className='card-title'>Category: {category}</h5>
        <h6 className='card-subtitle mb-2 text-muted'>Name: {name}</h6>
        <p className='card-text'>Description: {description}</p>
      </div>
    </div>
  );
};

export default CardForLastSeen;
