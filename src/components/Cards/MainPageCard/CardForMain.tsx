import React from 'react';
import { useHistory } from 'react-router-dom';

interface ICardForMain {
  name: string;
  logo: any;
  link: string;
}

const CardForMain: React.FC<ICardForMain> = ({ name, logo, link }) => {
  const history = useHistory();

  return (
    <div className='card card-main' onClick={() => history.push(link)}>
      <img src={logo} className='card-img-top' alt='...' />
      <div className='card-body card-body-main'>
        <h5 className='card-title'>{name}</h5>
      </div>
    </div>
  );
};

export default CardForMain;
