import React from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { ICharList } from '../../../pages/Characters/characters-interfaces';
import star from '../../../img/star.svg';
import { addDetailItemCharacter } from '../../../redux/actions/detail/detailActions';
import { addLastSeenItem } from '../../../redux/actions/lastSeen/lastSeenActions';
import { toggleFavoriteItem } from '../../../redux/actions/favorites/favorites';

interface ICardForChar {
  item: ICharList;
  isFavorite: boolean;
}

const CardForChar: React.FC<ICardForChar> = ({ item, isFavorite }) => {
  const dispatch = useDispatch();
  const handleClick = () => {
    dispatch(addDetailItemCharacter(item));
    dispatch(addLastSeenItem(item));
  };
  const addFavorites = () => dispatch(toggleFavoriteItem(item));
  const style = ['card'];
  if (isFavorite) {
    style.push('isFavorite');
  }
  console.log();
  return (
    <div className={style.join(' ')}>
      <div className='card-body'>
        <h5 className='card-title'>
          Name: <i>{item.name}</i>
        </h5>
        <h6 className='card-subtitle mb-2 text-muted'>Gender: {item.gender}</h6>
        <p className='card-text'>
          <span>Born: {item.name} </span>
          <span>Died: {item.died}</span>
        </p>
        <NavLink to='/characters/detail'>
          <span className='card-link' onClick={handleClick}>
            Просмотр
          </span>
        </NavLink>
      </div>
      <div className='star' onClick={addFavorites}>
        <img src={star} alt='' />
      </div>
    </div>
  );
};

export default React.memo(CardForChar);
