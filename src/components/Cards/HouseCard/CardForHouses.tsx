import React from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { IHousesList } from '../../../pages/Houses/Houses-interfaces';
import star from '../../../img/star.svg';
import { addDetailItemHouse } from '../../../redux/actions/detail/detailActions';
import { addLastSeenItem } from '../../../redux/actions/lastSeen/lastSeenActions';
import { toggleFavoriteItem } from '../../../redux/actions/favorites/favorites';

interface ICardForHouses {
  item: IHousesList;
  isFavorite: boolean;
}

const CardForHouses: React.FC<ICardForHouses> = ({ item, isFavorite }) => {
  const dispatch = useDispatch();
  const handleCLick = () => {
    dispatch(addDetailItemHouse(item));
    dispatch(addLastSeenItem(item));
  };
  const addFavorites = () => dispatch(toggleFavoriteItem(item));
  const style = ['card'];
  if (isFavorite) {
    style.push('isFavorite');
  }
  return (
    <div className={style.join(' ')}>
      <div className='card-body'>
        <h5 className='card-title'>
          Name: <i>{item.name}</i>
        </h5>
        <h6 className='card-subtitle mb-2 text-muted'>region: {item.region}</h6>
        <p className='card-text'>
          <span>words: {item.words} </span>
          {/* <span>seats: {item.seats.join(', ')} </span> */}
          <span>founded: {item.founded}</span>
        </p>
        <NavLink to='/houses/detail'>
          <span className='card-link' onClick={handleCLick}>
            Просмотр
          </span>
        </NavLink>
      </div>
      <div className='star' onClick={addFavorites}>
        <img src={star} alt='' />
      </div>
    </div>
  );
};

export default React.memo(CardForHouses);
