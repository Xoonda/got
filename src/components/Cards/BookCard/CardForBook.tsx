import React from 'react';
import { useDispatch } from 'react-redux';
import { NavLink } from 'react-router-dom';
import { ICardBook } from './CardBook-interfaces';
import star from '../../../img/star.svg';
import { addDetailItemBook } from '../../../redux/actions/detail/detailActions';
import { addLastSeenItem } from '../../../redux/actions/lastSeen/lastSeenActions';
import { toggleFavoriteItem } from '../../../redux/actions/favorites/favorites';

const Card: React.FC<ICardBook> = ({ isFavorite, item }) => {
  const dispatch = useDispatch();
  const handleCLick = () => {
    dispatch(addDetailItemBook(item));
    dispatch(addLastSeenItem(item));
  };
  const handlerFavorites = () => dispatch(toggleFavoriteItem(item));
  const style = ['card'];
  if (isFavorite) {
    style.push('isFavorite');
  }
  return (
    <div className={style.join(' ')}>
      <div className='card-body'>
        <h5 className='card-title'>
          Name: <i>{item.name}</i>
        </h5>
        <h6 className='card-subtitle mb-2 text-muted'>{item.authors.join(', ')}</h6>
        <p className='card-text'>
          <span>Pages: {item.numberOfPages}</span>
          <span>Publisher: {item.publisher}</span>
          <span>Country: {item.country}</span>
        </p>
        <NavLink to='/books/detail'>
          <span className='card-link' onClick={handleCLick}>
            Просмотр
          </span>
        </NavLink>
      </div>
      <div className='star' onClick={handlerFavorites}>
        <img src={star} alt='' />
      </div>
    </div>
  );
};

export default React.memo(Card);
