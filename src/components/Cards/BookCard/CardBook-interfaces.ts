import { IBookList } from '../../../pages/Books/Books-interfaces';

export interface ICardBook {
  isFavorite: boolean;
  item: IBookList;
}
